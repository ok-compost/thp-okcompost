Rails.application.routes.draw do
  root to: "results#new"
  resources :results, only: [:create, :new, :show]
  devise_for :users
  resources :users, except: [:index]

  resources :composts, except: [:index] do
    resources :pictures, only: [:create]
    resources :contributions, only: [:create, :update, :new]
    resources :timeslots, only: [:create, :destroy]
  end

  resources :conversations, only: [:create] do
    resources :messages, only: [:create, :index]
  end

  # error pages
  %w( 404 422 500 503 ).each do |code|
    get code, :to => "errors#show", :code => code
  end

end
