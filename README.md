![Page d'accueil](https://i.ibb.co/6PgMgqK/homepage.png)

<div align="center">
  <h1>OK-COMPOST</h1>
  <h3>Offrez de belles rencontres à vos épluchures...</h3>
</div>


> Une application web de mise en relation entre celles et ceux qui épluchent et celles et ceux qui compostent. Elle permet de réduire la quantité de déchets enfouis ou brûlés, de raccourcir le circuit du recyclage et de créer du lien entre personnes conscientes de la catastrophe environnementale. Le tout gratuitement et sans publicité. Bon, pour l'instant, sans public aussi.

 ![pipeline status](https://framagit.org/ok-compost/thp-okcompost/badges/staging/pipeline.svg)
 ![build status mockup](https://img.shields.io/badge/build-passing-success)
 ![heroku app status](https://img.shields.io/website?down_color=orange&down_message=offline%20-%20oups%20%21&label=Heroku%20app&up_color=brightgreen&up_message=online%20-%20yay%20%21&url=https%3A%2F%2Fok-compost.herokuapp.com%2F)

Cette application a été développée dans le cadre de la formation The Hacking Project - un bootcamp de développement web.

Elle permet de rechercher des composts en fonction de leur localisation, de leur composition (bio, accepte les agrumes...) et de se mettre en relation avec la ou les personne(s) qui les gère(nt). Elle intègre une localisation des composts, un système de messagerie et très peu de magenta !

## Site


### Formulaire de recherche
![formulaire de recherche](https://i.ibb.co/sR2m1bV/search-form.png)

### Résultats de recherche
![résultats avec carte](https://i.ibb.co/Vgf4LBQ/results-page.png)

### Page compost
![page compost](https://i.ibb.co/Mh0Xpc7/compost-page.png)

### Gestion des créneaux
![gestion des créneaux](https://i.ibb.co/JCJFY8y/dispos.png)

### Messagerie intégrée
![Messages](https://i.ibb.co/c1XH5G5/messages.png)

## Utilisation
Vidéo de démonstration [par ici](https://www.youtube.com/watch?v=LQWK-BGAgIE)
### Visite non connectée
* rechercher un compost
* voir les détails et les disponibilités du compost sélectionné

### Fourniture d'épluchures
* rechercher un compost
* voir les détails et proposer une contribution
* discuter par messages avec la personne qui le détient
* mettre à jour son profil

### Compostage d'épluchures
* créer et gérer son/ses composts
* gérer les propositions de contributions
* échanger par messages avec la personne fournissant les épluchures
* mettre à jour son profil

## Technologies utilisées
```bash
Rails version             5.2.4
Ruby version              2.5.1-p57 (x86_64-linux)
RubyGems version          3.0.6
Rack version              2.0.7
JavaScript Runtime        Node.js (V8)
Bundler                   2.0.2
```

Pour le front, [Bootstrap](https://getbootstrap.com/), agrémenté du thème[ Minty (Bootswatch)](https://bootswatch.com/minty/) et complété par les icônes [Fontawesome](https://fontawesome.com/).  
L'application est hébergée sur [Heroku](https://www.heroku.com/), le repository principal du projet sur [Framagit](https://framagit.org/).

### Gemmes principales
[Devise](https://github.com/plataformatec/devise) pour la gestion des sessions, [High-voltage](https://github.com/thoughtbot/high_voltage) pour les pages statiques, [Amazon AWS](https://github.com/aws/aws-sdk-ruby) pour le stockage, [Act As Taggable On](https://github.com/mbleigh/acts-as-taggable-on) pour la gestion des tags.


## Mise en place pour le développement

### Récupérer le code
Cloner le repository 
```bash
$ git clone gti@framagit.com:ok-compost/thp-okcompost.git
```

Se déplacer dans le dossier créé
```bash
$ cd thp-okcompost
```

Installer les outils et gemmes
```bash
$ bundle install
```

### Préparation de la base de données
Créer la base de données
```bash
$ rails db:create
```

Effectuer les migrations
```bash
$ rails db:migrate
```

Et la remplir
```bash
$ rails db:seed
```
_Note_ : un compte est créé lors du remplissage pour faciliter les tests
* _email_ : thp-okcompost@yopmail.com
* _mot de passe_ : motdepasse

### Server startup and URL to visit
Lancer le serveur
```bash
$ rails server
```
Le site est alors accessible [en local](localhost:3000).

## Tests
Pour l'instant, les test automatisés ne sont pas implémentés.

## Versions
* version 1.50 : ajout de la cartographie et de la messagerie
* version 1.00 : moteur de recherche opérationnel
* version 0.50 : CRUD sur les users et les composts
* version 0.00 : repo init

## Credits
On a beaucoup appris dans les [cours de The Hacking Project](https://www.thehackingproject.org/)\
Une ressource en or tout au long du projet : les [Rails guides](https://guides.rubyonrails.org/index.html)\
En complément, on a revu des morceaux des vidéos de [Grafikart](https://www.grafikart.fr/)\
Plus d'une fois, le [Mozilla Developper Network](https://developer.mozilla.org/en-US/) nous a bien servi !\
Et évidemment, un grand hommage à la mine d'or que constitue [Stack Overflow](https://stackoverflow.com/)\
Notre Readme est largement inspié de [celui-ci](https://github.com/iharsh234/WebApp)\

## La dream team
Cédric, Charlotte, Florence, Gaël, Margaux et Jean-Philippe.

## License
Contenu sous licence GNU-GPLv3. Voir `LICENCE` pour plus de détails.