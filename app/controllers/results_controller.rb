# frozen_string_literal: true

class ResultsController < ApplicationController
  include UsersHelper

  before_action :profile_enhancement, only: [:new]
  before_action :set_result, only: [:show]

  # GET /results/1
  def show
    @input_array = []
    @result.composts.each do |compost|
      # send input to the JS MapIni function with for each result line an array with : latitude, longitude and compost_id
      @input_array << [compost.latitude, compost.longitude, compost.id]
    end
  end

  # GET /results/new
  def new
    @result = Result.new
    @compositions = Compost.tag_counts_on('compositions')

    if current_user
      if !current_user.owned_composts.empty? && current_user.supplied_composts.empty?
        @composts = current_user.owned_composts.sample([3, current_user.owned_composts.size].min)
      elsif !current_user.supplied_composts.empty?
        @composts = current_user.supplied_composts.sample([3, current_user.supplied_composts.size].min)
      else
        @composts = Compost.all.sample(3)
      end
    else
      @composts = Compost.all.sample(3)
    end

    if @composts.size < 3
      (3 - @composts.size).times do
        @composts << Compost.all.sample
      end
    end
  end

  # POST /results
  def create
    @result = Result.new(result_params)
    respond_to do |format|
      if @result.save
        format.html { redirect_to @result, notice: 'Voici les résultats de votre recherche :' }
      else
        format.html { render :new }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_result
    @result = Result.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def result_params
    params.require(:result).permit(
      :district, :search_mode,
      composition: []
    )
  end

  # Method to ask the user to add more data to his profile on the welcome page ==> to move when we have the final welcome page
  def profile_enhancement
    if profile_completed?(50) == false
      flash.now[:notice] = "Votre profil n'est rempli qu'à #{current_user.profile_completion}%. A l'occasion, passez le compléter #{view_context.link_to('par ici', edit_user_path(current_user))}"
    end
  end
end
